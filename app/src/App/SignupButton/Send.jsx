// @flow

import React from 'react'

import R from 'ramda'

import type { SignupDataType } from '../SignupButton.jsx'
import type { SignupErrorsType } from '../SignupButton.jsx'

type PropsType = {
  onClick: () => void,
  signupData: SignupDataType,
  signupErrors: SignupErrorsType,
}

export const Send = (props: PropsType): React.Element<*> => {
  const {
    onClick,
    signupData,
    signupErrors,
  } = props

  return (
    <button
      id='inviteSend'
      onClick={onClick}
    >
      Send
    </button>
  )
}
