# Setup

1. Install docker and docker-compose
2. `make setup` to install dependencies
3. `make test` to run flow and unit tests
4. `docker-compose up` to start server. Server will be bound to your
   docker IP and available on port `8080`
5. To run integration tests you will need to start the selenium
   container. Run `docker-compose up` and in another terminal window,
run `map app-sh` to shell into the container, then run `make
test-integration`

Docker is not required to run the project, but will ease setup. To
manually install, see steps run in `Dockerfile.dev` and follow those to
configure your environment.

# Random thoughts

* Docker was used to easily spin up a selenium server for integration
  testing.

* Unit tests only cover one service file, normally I would write for
  React components as well but this had already taken a substantial
amount of time to write.

* Integration tests only cover the critical path and one failure path,
  normally they would be much more expansive.

* Judged that their was no need for Redux / MobX or Relay in this
  project. Pure React is enough.

* I write comments only when describing a reason for doing something in
  the code or if I'm documenting public APIs. Hopefully the code in this
project is easy enough to follow.

* I like to divide my components into UI (component itself) and
  operations on data (`services.js` files).

* I have not added any special handling around indicating the request is
  in flight. If I had a brief supplied as this one was then I would
raise that state and how the designer would want it displayed.
